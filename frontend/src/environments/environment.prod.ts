export const environment = {
    production: true,
    api_server: "/api",
    sync_interval_sec: 500,
    interval_refresh_sec: 800
};

import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {DataModule} from "../data/data.module";

@NgModule({
    declarations: [],
    imports: [
        CommonModule,
        DataModule,
    ],
    exports: []
})
export class DomainModule {
}

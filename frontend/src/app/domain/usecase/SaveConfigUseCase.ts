import {Callback, UseCase} from "../extra/UseCase";
import {ItemRepositoryService} from "../../data/repository/item-repository.service";
import {Injectable} from "@angular/core";
import {DomainModule} from "../domain.module";

@Injectable({
    providedIn: DomainModule
})
export class SaveConfigUseCase extends UseCase<any, Params> {
    constructor(
        private itemRepository: ItemRepositoryService
    ) {
        super()
    }

    build(params: Params, callback: Callback<any>): void {
        this.itemRepository.saveConfig(params.userId, params.config)
            .then(data => callback.onSuccess(data))
            .catch(err => callback.onError(err));
    }
}

export class Params {
    userId: string;
    config: any;

    constructor(userId: string, config: any) {
        this.userId = userId;
        this.config = config;
    }
}

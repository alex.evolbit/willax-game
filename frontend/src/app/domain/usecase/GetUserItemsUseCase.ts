import {Callback, UseCase} from "../extra/UseCase";
import {ItemRepositoryService} from "../../data/repository/item-repository.service";
import {Injectable} from "@angular/core";
import {DomainModule} from "../domain.module";
import {ItemUser} from "../types/ItemUser";

@Injectable({
    providedIn: DomainModule
})
export class GetUserItemsUseCase extends UseCase<ItemUser[], string> {
    constructor(
        private itemRepository: ItemRepositoryService
    ) {
        super()
    }

    build(params: string, callback: Callback<ItemUser[]>): void {
        this.itemRepository.getUserItems(params)
            .then(data => callback.onSuccess(data))
            .catch(err => callback.onError(err));
    }
}

import {Injectable} from "@angular/core";
import {DomainModule} from "../domain.module";
import {Callback, UseCase} from "../extra/UseCase";
import {ItemRepositoryService} from "../../data/repository/item-repository.service";
import {RawItemUser} from "../types/RawItemUser";

@Injectable({
    providedIn: DomainModule
})
export class GetUserScoreUseCase extends UseCase<RawItemUser[], string> {

    constructor(
        private itemRepository: ItemRepositoryService
    ) {
        super()
    }

    build(params: string, callback: Callback<RawItemUser[]>): void {
        this.itemRepository.getScore(params)
            .then(data => callback.onSuccess(data))
            .catch(err => callback.onError(err));
    }
}

import {Callback, UseCase} from "../extra/UseCase";
import {Injectable} from "@angular/core";
import {DomainModule} from "../domain.module";
import {ItemRepositoryService} from "../../data/repository/item-repository.service";
import {ItemUser} from "../types/ItemUser";

@Injectable({
    providedIn: DomainModule
})
export class AddItemToUserUseCase extends UseCase<ItemUser, Params> {
    constructor(
        private itemRepository: ItemRepositoryService
    ) {
        super()
    }

    build(params: Params, callback: Callback<ItemUser>): void {
        this.itemRepository.addItemToUser(params.userId, params.itemCode)
            .then(data => callback.onSuccess(data))
            .catch(err => callback.onError(err));
    }
}

export class Params {
    userId: string;
    itemCode: string;

    constructor(userId: string, itemCode: string) {
        this.userId = userId;
        this.itemCode = itemCode;
    }
}

import {Injectable} from "@angular/core";
import {Callback, UseCase} from "../extra/UseCase";
import {Item} from "../types/Item";
import {ItemRepositoryService} from "../../data/repository/item-repository.service";
import {DomainModule} from "../domain.module";


@Injectable({
    providedIn: DomainModule
})
export class CreateItemUseCase extends UseCase<Item, Params> {

    constructor(
        private itemRepository: ItemRepositoryService
    ) {
        super()
    }

    build(params: Params, callback: Callback<Item>): void {
        this.itemRepository.createItem(params.code, params.name, params.price, params.brand, params.supplier)
            .then(data => callback.onSuccess(data))
            .catch(err => callback.onError(err));
    }
}

export class Params {
    code: string;
    name: string;
    price: number;
    brand: string;
    supplier: string;

    constructor(code: string, name: string, price: number, brand: string, supplier: string) {
        this.code = code;
        this.name = name;
        this.price = price;
        this.brand = brand;
        this.supplier = supplier;
    }
}

import {Callback, UseCase} from "../extra/UseCase";
import {Item} from "../types/Item";
import {Injectable} from "@angular/core";
import {ItemRepositoryService} from "../../data/repository/item-repository.service";
import {ImportItem} from "../types/ImportItem";
import {DomainModule} from "../domain.module";

@Injectable({
    providedIn: DomainModule
})
export class ImportItemsUseCase extends UseCase<ImportItem[], Item[]> {
    constructor(
        private itemRepository: ItemRepositoryService
    ) {
        super()
    }

    build(params: Item[], callback: Callback<ImportItem[]>): void {
        this.itemRepository.importItems(params)
            .then(data => callback.onSuccess(data))
            .catch(err => callback.onError(err));
    }
}

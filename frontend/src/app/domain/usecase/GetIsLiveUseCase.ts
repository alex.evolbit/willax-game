import {Injectable} from "@angular/core";
import {DomainModule} from "../domain.module";
import {Callback, UseCase} from "../extra/UseCase";
import {ItemRepositoryService} from "../../data/repository/item-repository.service";

@Injectable({
    providedIn: DomainModule
})
export class GetIsLiveUseCase extends UseCase<boolean, null> {

    constructor(
        private itemRepository: ItemRepositoryService
    ) {
        super()
    }

    build(params: null, callback: Callback<boolean>): void {
        this.itemRepository.isLive()
            .then(data => callback.onSuccess(data))
            .catch(err => callback.onError(err));
    }
}

import {Callback, UseCase} from "../extra/UseCase";
import {ItemRepositoryService} from "../../data/repository/item-repository.service";
import {Injectable} from "@angular/core";
import {DomainModule} from "../domain.module";

@Injectable({
    providedIn: DomainModule
})
export class DeleteUserIdUseCase extends UseCase<void, string> {
    constructor(
        private itemRepository: ItemRepositoryService
    ) {
        super()
    }

    build(params: string, callback: Callback<void>): void {
        this.itemRepository.deleteUserById(params)
            .then(_ => callback.onSuccess())
            .catch(err => callback.onError(err));
    }
}

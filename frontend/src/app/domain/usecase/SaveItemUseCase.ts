import {Callback, UseCase} from "../extra/UseCase";
import {Item} from "../types/Item";
import {ItemRepositoryService} from "../../data/repository/item-repository.service";
import {Injectable} from "@angular/core";
import {DomainModule} from "../domain.module";

@Injectable({
    providedIn: DomainModule
})
export class SaveItemUseCase extends UseCase<Item, Item> {

    constructor(
        private itemRepository: ItemRepositoryService
    ) {
        super()
    }

    build(params: Item, callback: Callback<Item>): void {
        this.itemRepository.saveItem(params)
            .then(data => callback.onSuccess(data))
            .catch(err => callback.onError(err));
    }
}

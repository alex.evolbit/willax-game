import {Callback, UseCase} from "../extra/UseCase";
import {Injectable} from "@angular/core";
import {DomainModule} from "../domain.module";
import {ItemRepositoryService} from "../../data/repository/item-repository.service";

@Injectable({
    providedIn: DomainModule
})
export class SyncScoreUseCase extends UseCase<void, Params> {
    constructor(
        private itemRepository: ItemRepositoryService
    ) {
        super()
    }

    build(params: Params, callback: Callback<void>): void {
        this.itemRepository.synScore(params.userId, params.filename)
            .then(data => callback.onSuccess(data))
            .catch(err => callback.onError(err));
    }
}

export class Params {
    userId: string;
    filename: string;

    constructor(userId: string, filename: string) {
        this.userId = userId;
        this.filename = filename;
    }
}

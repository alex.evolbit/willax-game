import {Callback, UseCase} from "../extra/UseCase";
import {ItemRepositoryService} from "../../data/repository/item-repository.service";
import {Injectable} from "@angular/core";
import {DomainModule} from "../domain.module";

@Injectable({
    providedIn: DomainModule
})
export class GetUserIdUserCase extends UseCase<string, null> {
    constructor(
        private itemRepository: ItemRepositoryService
    ) {
        super()
    }

    build(params: null, callback: Callback<string>): void {
        this.itemRepository.getUserId()
            .then(data => callback.onSuccess(data))
            .catch(err => callback.onError(err));
    }
}

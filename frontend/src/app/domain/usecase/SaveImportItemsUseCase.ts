import {Callback, UseCase} from "../extra/UseCase";
import {ImportItem} from "../types/ImportItem";
import {Item} from "../types/Item";
import {ItemRepositoryService} from "../../data/repository/item-repository.service";
import {Injectable} from "@angular/core";
import {DomainModule} from "../domain.module";

@Injectable({
    providedIn: DomainModule
})
export class SaveImportItemsUseCase extends UseCase<Item[], ImportItem[]> {
    constructor(
        private itemRepository: ItemRepositoryService
    ) {
        super()
    }

    build(params: ImportItem[], callback: Callback<Item[]>): void {
        this.itemRepository.saveImportItems(params)
            .then(data => callback.onSuccess(data))
            .catch(err => callback.onError(err));
    }
}

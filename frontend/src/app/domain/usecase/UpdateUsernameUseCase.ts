import {Callback, UseCase} from "../extra/UseCase";
import {ItemRepositoryService} from "../../data/repository/item-repository.service";
import {Injectable} from "@angular/core";
import {DomainModule} from "../domain.module";

@Injectable({
    providedIn: DomainModule
})
export class UpdateUsernameUseCase extends UseCase<void, Params> {

    constructor(
        private itemRepository: ItemRepositoryService
    ) {
        super()
    }

    build(params: Params, callback: Callback<void>): void {
        this.itemRepository.updateUserName(params.userId, params.username)
            .then(data => callback.onSuccess(data))
            .catch(err => callback.onError(err));
    }
}

export class Params {
    userId: string;
    username: string;

    constructor(userId: string, username: string) {
        this.userId = userId;
        this.username = username;
    }
}

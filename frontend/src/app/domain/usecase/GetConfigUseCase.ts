import {Callback, UseCase} from "../extra/UseCase";
import {ItemRepositoryService} from "../../data/repository/item-repository.service";
import {Injectable} from "@angular/core";
import {DomainModule} from "../domain.module";

@Injectable({
    providedIn: DomainModule
})
export class GetConfigUseCase extends UseCase<any, string> {
    constructor(
        private itemRepository: ItemRepositoryService
    ) {
        super()
    }

    build(params: string, callback: Callback<any>): void {
        this.itemRepository.getConfig(params)
            .then(data => callback.onSuccess(data))
            .catch(err => callback.onError(err));
    }
}

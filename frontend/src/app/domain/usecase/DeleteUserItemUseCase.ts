import {Callback, UseCase} from "../extra/UseCase";
import {Injectable} from "@angular/core";
import {DomainModule} from "../domain.module";
import {ItemRepositoryService} from "../../data/repository/item-repository.service";

@Injectable({
    providedIn: DomainModule
})
export class DeleteUserItemUseCase extends UseCase<void, Params> {

    constructor(
        private itemRepository: ItemRepositoryService
    ) {
        super()
    }

    build(params: Params, callback: Callback<void>): void {
        this.itemRepository.deleteUserItem(params.userId, params.itemUserId)
            .then(data => callback.onSuccess(data))
            .catch(err => callback.onError(err));
    }
}

export class Params {
    userId: string;
    itemUserId: number;

    constructor(userId: string, itemUserId: number) {
        this.userId = userId;
        this.itemUserId = itemUserId;
    }
}


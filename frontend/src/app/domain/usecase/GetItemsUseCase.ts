import {Callback, UseCase} from "../extra/UseCase";
import {ItemRepositoryService} from "../../data/repository/item-repository.service";
import {Injectable} from "@angular/core";
import {GetItemsResponse} from "../types/GetItemsResponse";
import {DomainModule} from "../domain.module";

@Injectable({
    providedIn: DomainModule
})
export class GetItemsUseCase extends UseCase<GetItemsResponse, Params> {

    constructor(
        private itemRepository: ItemRepositoryService
    ) {
        super()
    }

    build(params: Params, callback: Callback<GetItemsResponse>): void {
        this.itemRepository.getItems(params.search, params.page, params.perPage, params.sortCol, params.sortDir)
            .then(data => callback.onSuccess(data))
            .catch(err => callback.onError(err));
    }
}

export class Params {
    search: string;
    page: number;
    perPage: number;
    sortCol: string;
    sortDir: string;

    constructor(search: string, page: number, perPage: number, sortCol: string, sortDir: string) {
        this.search = search;
        this.page = page;
        this.perPage = perPage;
        this.sortCol = sortCol;
        this.sortDir = sortDir;
    }
}

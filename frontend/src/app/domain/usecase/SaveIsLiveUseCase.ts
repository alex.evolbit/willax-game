import {Injectable} from "@angular/core";
import {DomainModule} from "../domain.module";
import {Callback, UseCase} from "../extra/UseCase";
import {ItemRepositoryService} from "../../data/repository/item-repository.service";

@Injectable({
    providedIn: DomainModule
})
export class SaveIsLiveUseCase extends UseCase<void, boolean> {

    constructor(
        private itemRepository: ItemRepositoryService
    ) {
        super()
    }

    build(params: boolean, callback: Callback<void>): void {
        this.itemRepository.saveIsLive(params)
            .then(data => callback.onSuccess(data))
            .catch(err => callback.onError(err));
    }
}

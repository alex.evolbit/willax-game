import {Callback, UseCase} from "../extra/UseCase";
import {User} from "../types/User";
import {Injectable} from "@angular/core";
import {DomainModule} from "../domain.module";
import {ItemRepositoryService} from "../../data/repository/item-repository.service";

@Injectable({
    providedIn: DomainModule
})
export class GetPlayersUseCase extends UseCase<User[], null> {
    constructor(
        private itemRepository: ItemRepositoryService
    ) {
        super()
    }

    build(params: null, callback: Callback<User[]>): void {
        this.itemRepository.getUsers()
            .then(data => callback.onSuccess(data))
            .catch(err => callback.onError(err));
    }
}

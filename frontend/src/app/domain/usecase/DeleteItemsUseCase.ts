import {Callback, UseCase} from "../extra/UseCase";
import {ItemRepositoryService} from "../../data/repository/item-repository.service";
import {Injectable} from "@angular/core";
import {DomainModule} from "../domain.module";

@Injectable({
    providedIn: DomainModule
})
export class DeleteItemsUseCase extends UseCase<void, string[]> {

    constructor(
        private itemRepository: ItemRepositoryService
    ) {
        super()
    }

    build(params: string[], callback: Callback<void>): void {
        this.itemRepository.deleteItems(params)
            .then(data => callback.onSuccess())
            .catch(err => callback.onError(err));
    }
}

export interface ImportItem {
    name: string;
    code: number;
    price: number;
    brand: string;
    isNew: boolean;
    update: boolean;
}

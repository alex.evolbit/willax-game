import {Item} from "./Item";

export interface ItemUser {
    id: number,
    pos: number,
    item: Item
}

import {Item} from "./Item";

export interface RawItemUser {
    id: number,
    itemcode: string,
    price: number
}

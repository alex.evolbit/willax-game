import {Item} from "./Item";
import {Pagination} from "./Pagination";

export interface GetItemsResponse {
    pagination: Pagination
    data: Item[]
}

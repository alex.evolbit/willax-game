export interface Item {
    name: string;
    code: string;
    price: number;
    brand: string;
    supplier: string;
}

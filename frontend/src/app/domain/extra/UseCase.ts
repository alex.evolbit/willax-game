export interface Callback<R> {
    onSuccess(response: R | null): void

    onError(exception: Error): void
}

export abstract class UseCase<T, Params> {
    abstract build(params: Params | null, callback: Callback<T>): void

    execute(params: Params | null, callback: Callback<T>) {
        this.build(params, callback)
    }
}

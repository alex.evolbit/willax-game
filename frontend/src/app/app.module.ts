import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {MaterialModule} from "./material.module";
import {SideNavComponent} from './presentation/components/side-nav/side-nav.component';
import {InventoryComponent} from './presentation/pages/inventory/inventory.component';
import {UploadExcelComponent} from './presentation/pages/upload-excel/upload-excel.component';
import {CreateItemComponent} from './presentation/pages/create-item/create-item.component';
import {ScanComponent} from './presentation/pages/scan/scan.component';
import {ShowScoreComponent} from './presentation/pages/show-score/show-score.component';
import {PlayersComponent} from './presentation/pages/players/players.component';
import {MAT_FORM_FIELD_DEFAULT_OPTIONS} from "@angular/material/form-field";
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {StandardDialogComponent} from './presentation/components/standard-dialog/standard-dialog.component';
import {DomainModule} from "./domain/domain.module";
import {DEFAULT_TIMEOUT, TimeoutInterceptor} from "./http.interceptor";


@NgModule({
    declarations: [
        AppComponent,
        SideNavComponent,
        InventoryComponent,
        UploadExcelComponent,
        CreateItemComponent,
        ScanComponent,
        ShowScoreComponent,
        PlayersComponent,
        StandardDialogComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        HttpClientModule,
        MaterialModule,
        FormsModule,
        ReactiveFormsModule,
        DomainModule,
    ],
    providers: [
        {provide: MAT_FORM_FIELD_DEFAULT_OPTIONS, useValue: {floatLabel: 'always'}},
        /*[{ provide: HTTP_INTERCEPTORS, useClass: TimeoutInterceptor, multi: true }],
        [{ provide: DEFAULT_TIMEOUT, useValue: 5000 }]*/
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}

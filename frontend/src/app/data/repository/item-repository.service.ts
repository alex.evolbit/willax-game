import {Injectable} from '@angular/core';
import {ItemRemoteDataSourceService} from "../storage/item-remote-data-source.service";
import {Item} from "../../domain/types/Item";
import {ItemRepository} from "../../domain/repository/ItemRepository";
import {DataModule} from "../data.module";
import {GetItemsResponse} from "../../domain/types/GetItemsResponse";
import {ImportItem} from "../../domain/types/ImportItem";
import {ItemLocalDataSourceService} from "../storage/item-local-data-source.service";
import {User} from "../../domain/types/User";
import {ItemUser} from "../../domain/types/ItemUser";
import {RawItemUser} from "../../domain/types/RawItemUser";


@Injectable({
    providedIn: DataModule
})
export class ItemRepositoryService implements ItemRepository {

    constructor(
        private itemRemoteDataSource: ItemRemoteDataSourceService,
        private itemLocalDataSourceService: ItemLocalDataSourceService
    ) {
    }

    getUserId(): Promise<string> {
        return this.itemLocalDataSourceService.getUserId();
    }

    deleteUserById(id: string): Promise<void> {
        return this.itemRemoteDataSource.deleteUserById(id);
    }

    getItems(search: string, page: number, perPage: number, sortCol: string, sortDir: string): Promise<GetItemsResponse> {
        return this.itemRemoteDataSource.getItems(search, page, perPage, sortCol, sortDir);
    }

    getItemById(id: string): Promise<Item | null> {
        return this.itemRemoteDataSource.getItemById(id);
    }

    createItem(code: string, name: string, price: number, brand: string, supplier: string): Promise<Item> {
        return this.itemRemoteDataSource.createItem(code, name, price, brand, supplier);
    }

    saveItem(item: Item): Promise<Item> {
        return this.itemRemoteDataSource.saveItem(item);
    }

    deleteItemById(id: string): Promise<void> {
        return this.itemRemoteDataSource.deleteItemById(id);
    }

    deleteItems(ids: string[]): Promise<void> {
        return this.itemRemoteDataSource.deleteItems(ids);
    }

    importItems(items: Item[]): Promise<ImportItem[]> {
        return this.itemRemoteDataSource.importItems(items);
    }

    saveImportItems(items: ImportItem[]): Promise<Item[]> {
        return this.itemRemoteDataSource.saveImportItems(items);
    }

    getUserName(userId: string): Promise<string> {
        return this.itemRemoteDataSource.getUserName(userId);
    }

    updateUserName(userId: string, username: string): Promise<void> {
        return this.itemRemoteDataSource.updateUserName(userId, username);
    }

    getUserItems(userId: string): Promise<ItemUser[]> {
        return this.itemRemoteDataSource.getUserItems(userId);
    }

    getScore(userId: string): Promise<any> {
        return this.itemRemoteDataSource.getScore(userId);
    }

    addItemToUser(userId: string, itemCode: string): Promise<ItemUser> {
        return this.itemRemoteDataSource.addItemToUser(userId, itemCode);
    }

    deleteUserItem(userId: string, itemUserId: number): Promise<void> {
        return this.itemRemoteDataSource.deleteUserItem(userId, itemUserId);
    }

    deleteUserItems(userId: string): Promise<void> {
        return this.itemRemoteDataSource.deleteUserItems(userId);
    }

    getUsers(): Promise<User[]> {
        return this.itemRemoteDataSource.getUsers();
    }

    synScore(userId: string, filename: string): Promise<void> {
        return this.itemRemoteDataSource.synScore(userId, filename);
    }

    isLive(): Promise<boolean> {
        return this.itemLocalDataSourceService.isLive();
    }

    saveIsLive(isLive: boolean): Promise<void> {
        return this.itemLocalDataSourceService.saveIsLive(isLive);
    }

    getConfig(userId: string): Promise<any> {
        return this.itemRemoteDataSource.getConfig(userId);
    }

    saveConfig(userId: string, config: any): Promise<any> {
        return this.itemRemoteDataSource.saveConfig(userId, config);
    }
}

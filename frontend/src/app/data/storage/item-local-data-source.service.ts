import {Injectable} from '@angular/core';
import {ItemLocalDataSource} from "../datasource/ItemLocalDataSource";
import {DataModule} from "../data.module";

const TOKEN_KEY = "token";
const IS_LIVE_KEY = "is_live";

@Injectable({
    providedIn: DataModule
})
export class ItemLocalDataSourceService implements ItemLocalDataSource {

    constructor() {
    }

    getUserId(): Promise<string> {
        return new Promise<string>((resolve, reject) => {
            let userId = localStorage.getItem(TOKEN_KEY) || null;
            if (userId == null) {
                userId = (Math.floor(Math.random() * 1000000)).toString();
                localStorage.setItem(TOKEN_KEY, userId);
            }
            resolve(userId);
        });
    }

    isLive(): Promise<boolean> {
        return new Promise<boolean>((resolve, reject) => {
            let isLive = localStorage.getItem(IS_LIVE_KEY) === 'yes';
            resolve(isLive);
        });
    }

    saveIsLive(isLive: boolean): Promise<void> {
        return new Promise<void>((resolve, reject) => {
            localStorage.setItem(IS_LIVE_KEY, isLive ? "yes" : "no");
            resolve();
        });
    }
}

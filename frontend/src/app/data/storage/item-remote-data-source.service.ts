import {Injectable} from '@angular/core';
import {ItemRemoteDataSource} from "../datasource/ItemRemoteDataSource";
import {Item} from "../../domain/types/Item";
import {DataModule} from "../data.module";
import {HttpClient, HttpErrorResponse, HttpHeaders} from "@angular/common/http";
import {environment} from "../../../environments/environment";
import {GetItemsResponse} from "../../domain/types/GetItemsResponse";
import {ImportItem} from "../../domain/types/ImportItem";
import {User} from "../../domain/types/User";
import {ItemUser} from "../../domain/types/ItemUser";
import {RawItemUser} from "../../domain/types/RawItemUser";

@Injectable({
    providedIn: DataModule
})
export class ItemRemoteDataSourceService implements ItemRemoteDataSource {

    constructor(
        private http: HttpClient
    ) {
    }

    private handleError(errorResponse: HttpErrorResponse) {
        let message = 'Something bad happened. Please try again later.';
        console.log(errorResponse)
        if (errorResponse && errorResponse.message) {
            message = errorResponse.message;
        }
        if (errorResponse && errorResponse.error && errorResponse.error.message) {
            message = errorResponse.error.message;
        }
        return new Error(message);
    }

    deleteUserById(id: string): Promise<void> {
        return new Promise<void>((resolve, reject) => {
            this.http
                .delete(environment.api_server + `/players/${id}`)
                .subscribe(
                    (response: any) => {
                        resolve();
                    },
                    err => reject(this.handleError(err))
                );
        });
    }

    getItemById(id: string): Promise<Item | null> {
        return new Promise<Item | null>((resolve, reject) => {
            this.http.get(environment.api_server + `/items/${id}`)
                .subscribe(
                    (response: any) => {
                        resolve(response.data as Item);
                    },
                    err => reject(this.handleError(err))
                );
        });
    }

    getItems(search: string, page: number, perPage: number, sortCol: string, sortDir: string): Promise<GetItemsResponse> {
        return new Promise<GetItemsResponse>((resolve, reject) => {
            this.http.get(environment.api_server + `/items?search=${search}&page=${page}&perPage=${perPage}&sortCol=${sortCol}&sortDir=${sortDir}`)
                .subscribe(
                    (response: any) => {
                        resolve(response as GetItemsResponse);
                    },
                    err => reject(this.handleError(err))
                );
        });
    }

    createItem(code: string, name: String, price: number, brand: string, supplier: string): Promise<Item> {
        return new Promise<Item>((resolve, reject) => {
            const data = {code, name, price, brand, supplier};
            this.http.post(environment.api_server + `/items/`, data)
                .subscribe(
                    (response: any) => {
                        resolve(response.data as Item);
                    },
                    err => reject(this.handleError(err))
                );
        });
    }

    saveItem(item: Item): Promise<Item> {
        return new Promise<Item>((resolve, reject) => {
            this.http.put(environment.api_server + `/items/${item.code}`, item)
                .subscribe(
                    (response: any) => {
                        resolve(response.data as Item);
                    },
                    err => reject(this.handleError(err))
                );
        });
    }

    deleteItemById(id: string): Promise<void> {
        return new Promise<void>((resolve, reject) => {
            this.http.delete(environment.api_server + `/items/${id}`)
                .subscribe(
                    (response: any) => {
                        resolve();
                    },
                    err => reject(this.handleError(err))
                );
        });
    }

    deleteItems(ids: string[]): Promise<void> {
        return new Promise<void>((resolve, reject) => {
            this.http.delete(environment.api_server + `/items`, {body: ids})
                .subscribe(
                    (response: any) => {
                        resolve();
                    },
                    err => reject(this.handleError(err))
                );
        });
    }

    importItems(items: Item[]): Promise<ImportItem[]> {
        return new Promise<ImportItem[]>((resolve, reject) => {
            this.http.post(environment.api_server + `/items/import`, items)
                .subscribe(
                    (response: any) => {
                        resolve(response.data as ImportItem[]);
                    },
                    err => reject(this.handleError(err))
                );
        });
    }

    saveImportItems(items: ImportItem[]): Promise<Item[]> {
        return new Promise<Item[]>((resolve, reject) => {
            this.http.post(environment.api_server + `/items/save-import`, items)
                .subscribe(
                    (response: any) => {
                        resolve(response.data as Item[]);
                    },
                    err => reject(this.handleError(err))
                );
        });
    }

    getUserName(userId: string): Promise<string> {
        return new Promise<string>((resolve, reject) => {
            this.http.get(environment.api_server + `/players/${userId}/username`)
                .subscribe(
                    (response: any) => {
                        resolve(response.data as string);
                    },
                    err => reject(this.handleError(err))
                );
        });
    }

    updateUserName(userId: string, username: string): Promise<void> {
        return new Promise<void>((resolve, reject) => {
            this.http.post(environment.api_server + `/players/${userId}/username`, {username})
                .subscribe(
                    (response: any) => {
                        resolve();
                    },
                    err => reject(this.handleError(err))
                );
        });
    }

    getUserItems(userId: string): Promise<ItemUser[]> {
        return new Promise<ItemUser[]>((resolve, reject) => {
            this.http.get(environment.api_server + `/players/${userId}/items`, {headers: new HttpHeaders({timeout: `${1000}`})})
                .subscribe(
                    (response: any) => {
                        resolve(response.data as ItemUser[]);
                    },
                    err => reject(this.handleError(err))
                );
        });
    }

    getScore(userId: string): Promise<any> {
        return new Promise<RawItemUser[]>((resolve, reject) => {
            this.http.get(environment.api_server + `/players/${userId}/score`, {headers: new HttpHeaders({timeout: `${1000}`})})
                .subscribe(
                    (response: any) => {
                        resolve(response.data);
                    },
                    err => reject(this.handleError(err))
                );
        });
    }

    addItemToUser(userId: string, itemCode: string): Promise<ItemUser> {
        return new Promise<ItemUser>((resolve, reject) => {
            this.http.post(environment.api_server + `/players/${userId}/items`, {code: itemCode})
                .subscribe(
                    (response: any) => {
                        resolve({id: response.id, item: response.data} as ItemUser);
                    },
                    err => reject(this.handleError(err))
                );
        });
    }

    deleteUserItem(userId: string, itemUserId: number): Promise<void> {
        return new Promise<void>((resolve, reject) => {
            this.http.delete(environment.api_server + `/players/${userId}/items/${itemUserId}`)
                .subscribe(
                    (response: any) => {
                        resolve();
                    },
                    err => reject(this.handleError(err))
                );
        });
    }

    deleteUserItems(userId: string): Promise<void> {
        return new Promise<void>((resolve, reject) => {
            this.http.delete(environment.api_server + `/players/${userId}/items/`)
                .subscribe(
                    (response: any) => {
                        resolve();
                    },
                    err => reject(this.handleError(err))
                );
        });
    }

    getUsers(): Promise<User[]> {
        return new Promise<User[]>((resolve, reject) => {
            this.http.get(environment.api_server + `/players`)
                .subscribe(
                    (response: any) => {
                        resolve(response.data as User[]);
                    },
                    err => reject(this.handleError(err))
                );
        });
    }

    synScore(userId: string, filename: string): Promise<void> {
        return new Promise<void>((resolve, reject) => {
            this.http.post(environment.api_server + `/players/${userId}/sync`, {filename})
                .subscribe(
                    (response: any) => {
                        resolve();
                    },
                    err => reject(this.handleError(err))
                );
        });
    }

    getConfig(userId: string): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            this.http.get(environment.api_server + `/players/${userId}/config`, {headers: new HttpHeaders({timeout: `${1000}`})})
                .subscribe(
                    (response: any) => {
                        resolve(response.data);
                    },
                    err => reject(this.handleError(err))
                );
        });
    }

    saveConfig(userId: string, config: any): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            this.http.post(environment.api_server + `/players/${userId}/config`, config)
                .subscribe(
                    (response: any) => {
                        resolve(response);
                    },
                    err => reject(this.handleError(err))
                );
        });
    }
}

import {TestBed} from '@angular/core/testing';

import {ItemRemoteDataSourceService} from './item-remote-data-source.service';

describe('ItemRemoteDataSourceService', () => {
    let service: ItemRemoteDataSourceService;

    beforeEach(() => {
        TestBed.configureTestingModule({});
        service = TestBed.inject(ItemRemoteDataSourceService);
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });
});

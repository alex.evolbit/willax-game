import {TestBed} from '@angular/core/testing';

import {ItemLocalDataSourceService} from './item-local-data-source.service';

describe('ItemLocalDataSourceService', () => {
    let service: ItemLocalDataSourceService;

    beforeEach(() => {
        TestBed.configureTestingModule({});
        service = TestBed.inject(ItemLocalDataSourceService);
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });
});

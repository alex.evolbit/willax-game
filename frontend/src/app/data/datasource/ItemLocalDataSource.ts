export interface ItemLocalDataSource {

    getUserId(): Promise<string>

    isLive(): Promise<boolean>;

    saveIsLive(isLive: boolean): Promise<void>;
}

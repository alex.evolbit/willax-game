import {Item} from "../../domain/types/Item";
import {GetItemsResponse} from "../../domain/types/GetItemsResponse";
import {ImportItem} from "../../domain/types/ImportItem";
import {User} from "../../domain/types/User";
import {ItemUser} from "../../domain/types/ItemUser";
import {RawItemUser} from "../../domain/types/RawItemUser";

export interface ItemRemoteDataSource {

    deleteUserById(id: string): Promise<void>

    getItems(search: string, page: number, perPage: number, sortCol: string, sortDir: string): Promise<GetItemsResponse>

    getItemById(id: string): Promise<Item | null>

    createItem(code: string, name: String, price: number, brand: string, supplier: string): Promise<Item>

    saveItem(item: Item): Promise<Item>

    deleteItemById(id: string): Promise<void>

    deleteItems(ids: string[]): Promise<void>

    importItems(items: Item[]): Promise<ImportItem[]>

    saveImportItems(items: ImportItem[]): Promise<Item[]>

    getUserName(userId: string): Promise<string>

    updateUserName(userId: string, username: string): Promise<void>

    getUserItems(userId: string): Promise<ItemUser[]>

    getScore(userId: string): Promise<any>

    addItemToUser(userId: string, itemCode: string): Promise<ItemUser>

    deleteUserItem(userId: string, itemUserId: number): Promise<void>

    deleteUserItems(userId: string): Promise<void>

    getUsers(): Promise<User[]>

    synScore(userId: string, filename: string): Promise<void>

    saveConfig(userId: string, config: any): Promise<any>

    getConfig(userId: string): Promise<any>
}

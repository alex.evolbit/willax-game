import {Component, OnInit, HostListener} from '@angular/core';
import {GetUserIdUserCase} from "./domain/usecase/GetUserIdUserCase";
import {Callback} from "./domain/extra/UseCase";
import {GetConfigUseCase} from "./domain/usecase/GetConfigUseCase";
import {Params as SaveConfigParams, SaveConfigUseCase} from "./domain/usecase/SaveConfigUseCase";
import {Params, UpdateUsernameUseCase} from "./domain/usecase/UpdateUsernameUseCase";


@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
    title = 'willax-game';
    userId: string | null = null;
    showSideBar = true;

    constructor(
        private getUserIdUserCase: GetUserIdUserCase,
        private getConfigUseCase: GetConfigUseCase,
        private updateUsernameUseCase: UpdateUsernameUseCase,
        private saveConfigUseCase: SaveConfigUseCase
    ) {

    }

    ngOnInit(): void {
        const self = this;
        this.getUserIdUserCase.build(null, {
            onSuccess(userId: string) {
                self.userId = userId;
                self.validateConfig(userId);
            },
            onError(exception: Error) {
                console.error(exception);
                alert(exception)
            }
        } as Callback<string>);

        this.showSideBar = window.innerWidth >= 900;
    }

    @HostListener('window:resize', ['$event'])
    onWindowResize() {
        this.showSideBar = window.innerWidth >= 900;
    }

    private validateConfig(userId: string) {
        const self = this;
        this.getConfigUseCase.build(userId, {
            onSuccess(response: any) {
            },
            onError(err: Error) {
                self.createUser(userId);
            }
        } as Callback<any>);
    }

    private createUser(userId: string) {
        const self = this;

        const params = {
            userId: userId,
            username: `Player ${userId}`
        } as Params;
        this.updateUsernameUseCase.build(params, {
            onSuccess() {
                self.createConfig(userId);
            },
            onError(err: Error) {
                console.log(err);
            }
        } as Callback<void>);
    }

    private createConfig(userId: string) {
        const config = {
            score_filename: `player_${userId}.txt`,
            is_live: false
        };
        const params = {
            userId,
            config
        } as SaveConfigParams;

        this.saveConfigUseCase.build(params, {
            onSuccess(response: any) {

            },
            onError(err: Error) {
                console.log(err);
            }
        } as Callback<any>);
    }
}

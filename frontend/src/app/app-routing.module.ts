import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {InventoryComponent} from "./presentation/pages/inventory/inventory.component";
import {UploadExcelComponent} from "./presentation/pages/upload-excel/upload-excel.component";
import {ScanComponent} from "./presentation/pages/scan/scan.component";
import {CreateItemComponent} from "./presentation/pages/create-item/create-item.component";
import {ShowScoreComponent} from "./presentation/pages/show-score/show-score.component";
import {PlayersComponent} from "./presentation/pages/players/players.component";


const routes: Routes = [
    {path: '', redirectTo: '/inventory', pathMatch: 'full'},
    {path: 'inventory', component: InventoryComponent},
    {path: 'upload-excel', component: UploadExcelComponent},
    {path: 'create-item', component: CreateItemComponent},
    {path: 'edit-item/:id', component: CreateItemComponent},
    {path: 'scan/:userId', component: ScanComponent},
    {path: 'show-score/:userId', component: ShowScoreComponent},
    {path: 'players', component: PlayersComponent},
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {
}

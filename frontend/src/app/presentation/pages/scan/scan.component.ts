import {Component, OnInit, ViewChild, OnDestroy, HostListener, ElementRef} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {Params, UpdateUsernameUseCase} from "../../../domain/usecase/UpdateUsernameUseCase";
import {Callback} from "../../../domain/extra/UseCase";
import {MatSnackBar} from "@angular/material/snack-bar";
import {GetUsernameUseCase} from "../../../domain/usecase/GetUsernameUseCase";
import {GetUserItemsUseCase} from "../../../domain/usecase/GetUserItemsUseCase";
import {Params as ParamsAdd, AddItemToUserUseCase} from "../../../domain/usecase/AddItemToUserUseCase";
import {MatTable} from "@angular/material/table";
import {StandardDialogComponent} from "../../components/standard-dialog/standard-dialog.component";
import {MatDialog} from "@angular/material/dialog";
import {Params as ParamsDel, DeleteUserItemUseCase} from "../../../domain/usecase/DeleteUserItemUseCase";
import {DeleteUserItemsUseCase} from "../../../domain/usecase/DeleteUserItemsUseCase";
import {MatSlideToggleChange} from "@angular/material/slide-toggle";
import {ConfigService} from "../../services/config.service";
import {ItemUser} from "../../../domain/types/ItemUser";
import {GetConfigUseCase} from "../../../domain/usecase/GetConfigUseCase";
import {SaveConfigUseCase, Params as SaveConfigParams} from "../../../domain/usecase/SaveConfigUseCase";
import {environment} from "../../../../environments/environment";


declare const onScan: any;


@Component({
    selector: 'app-scan',
    templateUrl: './scan.component.html',
    styleUrls: ['./scan.component.scss']
})
export class ScanComponent implements OnInit, OnDestroy {
    displayedColumns: string[] = ['pos', 'code', 'name', 'price', 'brand', 'supplier', 'options'];
    dataSource: ItemUser[] = [];

    userId: string | null = null;
    username = "";
    itemCode = "";
    total = "";
    filename = "";
    ftpStatus = "No enviado";
    statusClass = "";
    isLive = false;
    isFtpSync = false;

    //Auto refresh
    intervalRef = 0;
    autoRefresh = false;

    @ViewChild(MatTable)
    table = {} as MatTable<ItemUser>;


    @ViewChild("fileNameFtpInput")
    fileNameFtpInput = {} as ElementRef<HTMLInputElement>;
    private readonly scanListener;

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private snackBar: MatSnackBar,
        //public configService: ConfigService,
        public dialog: MatDialog,
        private getUsernameUseCase: GetUsernameUseCase,
        private updateUsernameUseCase: UpdateUsernameUseCase,
        private getUserItemsUseCase: GetUserItemsUseCase,
        private addItemToUserUseCase: AddItemToUserUseCase,
        private deleteUserItemUseCase: DeleteUserItemUseCase,
        private deleteUserItemsUseCase: DeleteUserItemsUseCase,
        private getConfigUseCase: GetConfigUseCase,
        private saveConfigUseCase: SaveConfigUseCase
    ) {
        //this.router.routeReuseStrategy.shouldReuseRoute = () => false;

        const self = this;
        this.scanListener = function (event: any) {
            if (event.detail.scanCode) {
                self.itemCode = event.detail.scanCode;
                self.onSearch();
            }
        };
    }

    ngOnInit(): void {
        this.loadItems();
        onScan.attachTo(document);
        document.addEventListener('scan', this.scanListener);
    }

    ngOnDestroy(): void {
        //this.configService.stopFtpSync();

        this.disableAutoRefresh();

        document.removeEventListener('scan', this.scanListener);
        onScan.detachFrom(document);
    }

    private loadItems() {
        this.route.params.subscribe(params => {
            //const userId = this.route.snapshot.paramMap.get('userId');
            const userId = params['userId'];
            if (userId != null) {
                this.userId = userId;
                //this.filename = "game_" + this.userId + ".txt";
                this.getConfig(userId);
                //this.getUsername(userId);
                this.getUserItems(userId);
                /*if (this.configService.isFtpSync()) {
                    this.startFtpSync();
                }*/
            }
        });
    }


    private getUsername(userId: string) {
        const self = this;
        this.getUsernameUseCase.build(userId, {
            onSuccess(response: string) {
                self.username = response;
            },
            onError(err: Error) {
                console.log(err);
            }
        } as Callback<string>);
    }

    onUpdateUsername() {
        /*const self = this;

        const params = {
            userId: this.userId,
            username: this.username
        } as Params;
        this.updateUsernameUseCase.build(params, {
            onSuccess() {
                self.snackBar.open('Guardado', 'Cerrar', {duration: 5000, verticalPosition: 'top'});
            },
            onError(err: Error) {
                console.log(err);
            }
        } as Callback<void>);*/

        if (this.userId == null)
            return;

        if (this.fileNameFtpInput.nativeElement.value !== this.filename) {
            const dialogRef = this.dialog.open(StandardDialogComponent, {
                data: {
                    title: 'Eliminar',
                    content: '¿Estás seguro de cambiar el nombre de archivo?',
                },
            });
            dialogRef.afterClosed().subscribe(result => {
                if (result) {
                    if (this.userId != null) {
                        const filename = this.fileNameFtpInput.nativeElement.value;
                        this.saveConfig(this.userId, {
                            username: this.username,
                            score_filename: filename
                        });
                    }
                } else {
                    this.fileNameFtpInput.nativeElement.value = this.filename;
                }
            });
        } else {
            this.saveConfig(this.userId, {username: this.username});
        }
    }

    onSearch() {
        this.addItemToUser();
    }

    getUserItems(userId: string) {
        const self = this;
        this.getUserItemsUseCase.build(userId, {
            onSuccess(items: ItemUser[]) {
                const len = items.length;
                self.dataSource = items.map((item, ind) => {
                    item.pos = len - ind;
                    return item;
                });
                self.calculateTotal();
            },
            onError(err: Error) {
                console.log(err);
            }
        } as Callback<ItemUser[]>);
    }

    addItemToUser() {
        //console.log(onScan);
        //console.log("this.itemCode", this.itemCode);
        const self = this;

        const params = {
            userId: this.userId,
            itemCode: this.itemCode
        } as ParamsAdd;
        this.addItemToUserUseCase.build(params, {
            onSuccess(itemUser: ItemUser) {
                itemUser.pos = self.dataSource.length + 1;
                self.dataSource.unshift(itemUser);
                self.itemCode = "";
                self.table.renderRows();
                self.calculateTotal();
            },
            onError(err: Error) {
                self.itemCode = "";
                self.snackBar.open(err.message, 'Cerrar', {
                    duration: 45000,
                    verticalPosition: 'top',
                    panelClass: 'snackError'
                });
            }
        } as Callback<ItemUser>)
    }

    private calculateTotal() {
        let total = 0;
        for (const itemUser of this.dataSource) {
            total += itemUser.item.price;
        }

        this.total = total.toFixed(2);
    }

    onItemDelete(itemUserId: number, index: number) {
        const dialogRef = this.dialog.open(StandardDialogComponent, {
            data: {
                title: 'Eliminar',
                content: '¿Estás seguro de eliminar el item?',
            },
        });
        dialogRef.afterClosed().subscribe(result => {
            if (result)
                this.deleteItem(itemUserId, index);
        });
    }

    deleteItem(itemUserId: number, index: number) {
        const self = this;

        const params = {
            userId: this.userId,
            itemUserId: itemUserId
        } as ParamsDel;
        this.deleteUserItemUseCase.build(params, {
            onSuccess() {
                self.dataSource.splice(index, 1);
                self.table.renderRows();
                self.calculateTotal();
            },
            onError(err: Error) {
                self.snackBar.open(err.message, 'Cerrar', {duration: 5000, verticalPosition: 'top'});
            }
        } as Callback<void>)
    }

    onReloadItems() {
        this.loadItems();
    }

    onDeleteItems() {
        const dialogRef = this.dialog.open(StandardDialogComponent, {
            data: {
                title: 'Eliminar',
                content: '¿Estás seguro de eliminar la lista?',
            },
        });
        dialogRef.afterClosed().subscribe(result => {
            if (result)
                this.deleteItems();
        });
    }

    deleteItems() {
        const self = this;

        if (this.userId == null)
            return;

        this.deleteUserItemsUseCase.build(this.userId, {
            onSuccess() {
                self.dataSource = [];
                self.table.renderRows();
                self.calculateTotal();
            },
            onError(err: Error) {
                self.snackBar.open(err.message, 'Cerrar', {duration: 5000, verticalPosition: 'top'});
            }
        } as Callback<void>)
    }

    public onActivateFtpChange(evt: MatSlideToggleChange) {
        if (this.userId == null)
            return;

        this.isFtpSync = evt.checked;
        //this.saveConfig(this.userId, {is_ftp_sync: this.isFtpSync});


        const self = this;
        const params = {
            userId: this.userId,
            config: {
                is_ftp_sync: this.isFtpSync
            }
        } as SaveConfigParams;
        this.saveConfigUseCase.build(params, {
            onSuccess(response: any) {
                self.ftpStatus = "Actualizado";
                self.statusClass = 'success';
            },
            onError(err: Error) {
                console.log(err);
                self.ftpStatus = "Error: " + err.message;
                self.statusClass = 'error';
                self.isFtpSync = !evt.checked;
            }
        } as Callback<any>);

        /*if (evt.checked) {
            this.startFtpSync()
        } else {
            this.stopFtpSync();
        }*/
    }

    /*private startFtpSync() {
        const self = this;
        if (this.userId == null)
            return;

        this.configService.startFtpSync(this.userId, this.filename, {
            onSuccess() {
                self.ftpStatus = "Actualizado";
                self.statusClass = 'success';
            },
            onError(err: Error) {
                console.log(err);
                self.ftpStatus = "Error: " + err.message;
                self.statusClass = 'error';
            }
        } as Callback<void>);
    }

    private stopFtpSync() {
        this.configService.stopFtpSync();
    }*/

    onActivateLiveChange(evt: MatSlideToggleChange) {
        if (this.userId == null)
            return;

        //this.saveConfig(this.userId, {is_live: evt.checked, score_filename: this.filename});
        this.saveConfig(this.userId, {is_live: evt.checked});
    }

    onActivateAutoRefresh(evt: MatSlideToggleChange) {
        if (this.userId == null)
            return;

        if (evt.checked) {
            this.enableAutoRefresh();
        } else {
            this.disableAutoRefresh();
        }
    }

    private enableAutoRefresh() {
        this.disableAutoRefresh();
        this.intervalRef = setInterval(() => {
            if (this.userId)
                this.getUserItems(this.userId)
        }, environment.interval_refresh_sec);
    }

    private disableAutoRefresh() {
        if (this.intervalRef > 0) {
            clearInterval(this.intervalRef);
        }
    }

    private getConfig(userId: string) {
        const self = this;
        this.getConfigUseCase.build(userId, {
            onSuccess(response: any) {
                console.log(response);
                if (typeof response.is_live !== 'undefined') {
                    self.isLive = response.is_live;
                }
                if (typeof response.is_ftp_sync !== 'undefined') {
                    self.isFtpSync = response.is_ftp_sync;
                }
                if (typeof response.score_filename !== 'undefined' && response.score_filename) {
                    self.filename = response.score_filename;
                } else {
                    self.filename = "game_" + self.userId + ".txt";
                }
                if (typeof response.username !== 'undefined') {
                    self.username = response.username;
                }
            },
            onError(err: Error) {
                console.log(err);
            }
        } as Callback<any>);
    }

    private saveConfig(userId: string, config: any) {
        if (config.score_filename)
            config.score_filename = config.score_filename.replace(" ", "_");

        const self = this;
        const params = {
            userId,
            config
        } as SaveConfigParams;
        this.saveConfigUseCase.build(params, {
            onSuccess(response: any) {
                if (config.score_filename) {
                    self.filename = config.score_filename;
                }
            },
            onError(err: Error) {
                console.log(err);
            }
        } as Callback<any>);
    }

    @HostListener('document:keydown.control.r')
    doSomething() {
        this.onReloadItems();
    }
}

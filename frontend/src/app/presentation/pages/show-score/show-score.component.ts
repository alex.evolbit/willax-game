import {Component, ElementRef, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {GetUserItemsUseCase} from "../../../domain/usecase/GetUserItemsUseCase";
import {Callback} from "../../../domain/extra/UseCase";
import {ActivatedRoute} from "@angular/router";
import {GetUsernameUseCase} from "../../../domain/usecase/GetUsernameUseCase";
import {ConfigService} from "../../services/config.service";
import {ItemUser} from "../../../domain/types/ItemUser";
import {environment} from "../../../../environments/environment";
import {GetConfigUseCase} from "../../../domain/usecase/GetConfigUseCase";
import {AddItemToUserUseCase, Params as ParamsAdd} from "../../../domain/usecase/AddItemToUserUseCase";
import {GetUserScoreUseCase} from "../../../domain/usecase/GetUserScoreUseCase";
import {RawItemUser} from "../../../domain/types/RawItemUser";

declare const onScan: any;

@Component({
    selector: 'app-show-score',
    templateUrl: './show-score.component.html',
    styleUrls: ['./show-score.component.scss']
})
export class ShowScoreComponent implements OnInit, OnDestroy {

    username = "";
    userId: string | null = null;
    total = "";
    intervalRef = 0;
    isLive = false;

    @ViewChild('page')
    page = {} as ElementRef;
    elem: any;

    private readonly scanListener;

    constructor(
        private route: ActivatedRoute,
        private elementRef: ElementRef,
        public configService: ConfigService,
        private getUsernameUseCase: GetUsernameUseCase,
        //private getUserItemsUseCase: GetUserItemsUseCase,
        private getUserScoreUseCase: GetUserScoreUseCase,
        //private getConfigUseCase: GetConfigUseCase,
        private addItemToUserUseCase: AddItemToUserUseCase,
    ) {
        const self = this;
        this.scanListener = function (event: any) {
            if (event.detail.scanCode && self.userId) {
                self.addItemToUser(event.detail.scanCode);
            }
        };
    }

    ngOnInit(): void {
        const userId = this.route.snapshot.paramMap.get('userId');
        if (userId != null) {
            this.userId = userId;
        }
        this.intervalRef = setInterval(() => this.activateRealTime(), environment.interval_refresh_sec);
        this.openFullScreen();

        // Scan bar
        onScan.attachTo(document);
        document.addEventListener('scan', this.scanListener);
    }

    ngOnDestroy(): void {
        if (this.intervalRef > 0) {
            clearInterval(this.intervalRef);
        }

        // Scan bar7613036552554

        document.removeEventListener('scan', this.scanListener);
        onScan.detachFrom(document);
    }

    private activateRealTime() {
        if (this.userId != null) {
            //this.getConfig(this.userId);
            //this.getUsername(this.userId);
            this.getUserItems(this.userId);
        }
    }

    /*private getConfig(userId: string) {
        const self = this;
        this.getConfigUseCase.build(userId, {
            onSuccess(response: any) {
                if (typeof response.is_live !== 'undefined') {
                    self.isLive = response.is_live;
                }
                if (typeof response.username !== 'undefined') {
                    self.username = response.username;
                }
            },
            onError(err: Error) {
                console.log(err);
            }
        } as Callback<any>);
    }*/

    getUserItems(userId: string) {
        const self = this;

        this.getUserScoreUseCase.build(userId, {
            onSuccess(data: any) {
                //self.total = score.toFixed(2);
                if (typeof data.user.is_live !== 'undefined') {
                    self.isLive = data.user.is_live;
                }
                if (typeof data.user.username !== 'undefined') {
                    self.username = data.user.username;
                }
                self.calculateTotal(data.itemUsers);
            },
            onError(err: Error) {
                console.log(err);
            }
        } as Callback<RawItemUser[]>);

        /*this.getUserItemsUseCase.build(userId, {
            onSuccess(itemUsers: ItemUser[]) {
                self.calculateTotal(itemUsers);
            },
            onError(err: Error) {
                console.log(err);
            }
        } as Callback<ItemUser[]>);*/
    }

    private calculateTotal(items: RawItemUser[]) {
        let total = 0;
        for (const item of items) {
            //console.log(item['Item']['price']);
            total += item.price;
        }

        this.total = total.toFixed(2);
    }

    /*private calculateTotal(itemUsers: ItemUser[]) {
        let total = 0;
        for (const itemUser of itemUsers) {
            total += itemUser.item.price;
        }

        this.total = total.toFixed(2);
    }*/

    openFullScreen() {
        this.elem = this.elementRef.nativeElement.querySelector('.page');
        if (this.elem.requestFullscreen) {
            this.elem.requestFullscreen();
        } else if (this.elem.mozRequestFullScreen) {
            /* Firefox */
            this.elem.mozRequestFullScreen();
        } else if (this.elem.webkitRequestFullscreen) {
            /* Chrome, Safari and Opera */
            this.elem.webkitRequestFullscreen();
        } else if (this.elem.msRequestFullscreen) {
            /* IE/Edge */
            this.elem.msRequestFullscreen();
        }
    }

    // Scan Bar
    addItemToUser(itemCode: string) {
        const self = this;

        const params = {
            userId: this.userId,
            itemCode: itemCode
        } as ParamsAdd;
        this.addItemToUserUseCase.build(params, {
            onSuccess(itemUser: ItemUser) {
            },
            onError(err: Error) {
                console.error(err);
            }
        } as Callback<ItemUser>)
    }
}

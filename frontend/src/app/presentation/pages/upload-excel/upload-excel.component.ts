import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import * as XLSX from 'xlsx';
import {Item} from "../../../domain/types/Item";
import {ImportItem} from "../../../domain/types/ImportItem";
import {ImportItemsUseCase} from "../../../domain/usecase/ImportItemsUseCase";
import {Callback} from "../../../domain/extra/UseCase";
import {PageEvent} from "@angular/material/paginator";
import {MatSlideToggleChange} from "@angular/material/slide-toggle";
import {SaveImportItemsUseCase} from "../../../domain/usecase/SaveImportItemsUseCase";
import {MatSnackBar} from "@angular/material/snack-bar";

@Component({
    selector: 'app-upload-excel',
    templateUrl: './upload-excel.component.html',
    styleUrls: ['./upload-excel.component.scss']
})
export class UploadExcelComponent implements OnInit {
    displayedColumns: string[] = ['code', 'name', 'price', 'brand', 'supplier', 'options'];
    dataSource: ImportItem[] = [];
    items: ImportItem[] = [];

    page = 1;
    perPage = 10;
    total = 0;
    pageSizeOptions: number[] = [5, 10, 25, 100];

    //private spread;
    //private excelIO;

    @ViewChild('uploadInput')
    uploadInput = {} as ElementRef;

    constructor(
        private snackBar: MatSnackBar,
        private importItemsUseCase: ImportItemsUseCase,
        private saveImportItemsUseCase: SaveImportItemsUseCase
    ) {
        //this.spread = new GC.Spread.Sheets.Workbook();
        //this.excelIO = new Excel.IO();
    }

    ngOnInit(): void {
    }

    onLoadFile() {
        this.uploadInput.nativeElement.click();
    }

    onDownloadFormat() {
        window.open("assets/upload_format.xlsx", "_blank");
    }

    onFileChange(event: any) {
        /* wire up file reader */
        const target: DataTransfer = <DataTransfer>(event.target);
        if (target.files.length !== 1) {
            throw new Error('Cannot use multiple files');
        }
        const reader: FileReader = new FileReader();
        reader.readAsBinaryString(target.files[0]);
        reader.onload = (e: any) => {
            const binarystr: string = e.target.result;
            const wb: XLSX.WorkBook = XLSX.read(binarystr, {type: 'binary'});

            const wsname: string = wb.SheetNames[0];
            const ws: XLSX.WorkSheet = wb.Sheets[wsname];

            const header = ["code", "name", "price", "brand", "supplier"];
            const data = XLSX.utils.sheet_to_json(ws, {header: header, blankrows: false});

            const importList = [] as Item[];
            for (const ind in data) {
                if (ind == "0") continue;

                const item = data[ind] as Item;
                importList.push(item);
            }

            const self = this;
            this.importItemsUseCase.build(importList, {
                onSuccess(response: ImportItem[]) {
                    self.items = response;

                    for (const item of self.items) {
                        if (!item.isNew) item.update = true;
                    }
                    self.paginate();
                },

                onError(exception: Error) {
                    console.error(exception);
                }
            }  as Callback<ImportItem[]>)
        };
    }

    paginate() {
        this.total = this.items.length;

        const start = (this.page - 1) * this.perPage;
        const end = start + this.perPage;
        this.dataSource = this.items.slice(start, end);
    }

    onPage(event: PageEvent) {
        this.perPage = event.pageSize;
        this.page = event.pageIndex + 1;
        this.paginate();
    }

    onSlideChange(ev: MatSlideToggleChange, importItem: ImportItem) {
        importItem.update = ev.checked;
    }

    onSaveImport() {
        const self = this;
        this.saveImportItemsUseCase.build(this.items, {
            onSuccess(response: Item[]) {
                //console.log(response)
                self.snackBar.open('Guardado', 'Cerrar', {duration: 5000, verticalPosition: 'top'});
            },

            onError(exception: Error) {
                console.error(exception);
            }
        } as Callback<Item[]>)
    }
}

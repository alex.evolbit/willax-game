import {AfterViewInit, Component, ElementRef, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {Location} from '@angular/common'
import {GetItemByIdUseCase} from "../../../domain/usecase/GetItemByIdUseCase";
import {Callback} from "../../../domain/extra/UseCase";
import {Item} from "../../../domain/types/Item";
import {Validators, FormBuilder, FormGroup} from "@angular/forms";
import {CreateItemUseCase, Params} from "../../../domain/usecase/CreateItemUseCase";
import {MatSnackBar} from "@angular/material/snack-bar";
import {SaveItemUseCase} from "../../../domain/usecase/SaveItemUseCase";

declare const onScan: any;

@Component({
    selector: 'app-create-item',
    templateUrl: './create-item.component.html',
    styleUrls: ['./create-item.component.scss']
})
export class CreateItemComponent implements OnInit, OnDestroy {
    isNew = true;
    title = "";
    item = {
        code: '',
        name: '',
        price: 0,
        brand: '',
        supplier: '',
    } as Item;

    itemForm: FormGroup;
    private readonly scanListener;

    @ViewChild('codeInput')
    codeInput = {} as ElementRef;

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private location: Location,
        private formBuilder: FormBuilder,
        private snackBar: MatSnackBar,
        private getItemByIdUseCase: GetItemByIdUseCase,
        private createItemUseCase: CreateItemUseCase,
        private saveItemUseCase: SaveItemUseCase,
    ) {
        this.itemForm = this.formBuilder.group({
            code: ['', [Validators.required, Validators.minLength(1)]],
            name: ['', [Validators.required, Validators.minLength(1)]],
            price: ['', [Validators.required, Validators.minLength(1)]],
            brand: ['', [Validators.required, Validators.minLength(1)]],
            supplier: ['', [Validators.required, Validators.minLength(1)]],
        });
        const self = this;
        this.scanListener = function (event: any) {
            if (self.isNew && event.detail.scanCode) {
                self.codeInput.nativeElement.value = event.detail.scanCode;
            }
        };
    }

    ngOnInit(): void {
        const id = this.route.snapshot.paramMap.get('id');
        if (this.isNew = id == null) {
            this.title = "Crear artículo"
        } else {
            this.title = "Editar artículo";
            this.loadItem(id);
        }

        // Scan
        onScan.attachTo(document);
        document.addEventListener('scan', this.scanListener);
    }

    ngOnDestroy(): void {
        // Scan
        document.removeEventListener('scan', this.scanListener);
        onScan.detachFrom(document);
    }

    loadItem(id: string) {
        const self = this;

        this.getItemByIdUseCase.build(id, {
            onSuccess(response: Item) {
                self.item = response;
                self.itemForm = self.formBuilder.group(response);
            },

            onError(exception: Error) {
                console.error(exception);
            }
        } as Callback<Item>);
    }

    save() {
        if (this.isNew) {
            if (this.itemForm.valid)
                this.createItem(this.itemForm.value)
        } else
            this.saveItem(this.itemForm.value)
    }

    createItem(data: Item) {
        const params = new Params(data.code, data.name, data.price, data.brand, data.supplier);

        const self = this;
        this.createItemUseCase.build(params, {
            onSuccess(response: Item) {
                self.snackBar.open('Creado', 'Cerrar', {duration: 5000, verticalPosition: 'top'});
                self.item = response;
                self.router.navigate(['edit-item', self.item.code])
            },

            onError(exception: Error) {
                console.error(exception);
            }
        })
    }

    saveItem(data: Item) {
        const self = this;
        this.saveItemUseCase.build(data, {
            onSuccess(response: Item) {
                self.snackBar.open('Guardado', 'Cerrar', {duration: 5000, verticalPosition: 'top'});
                self.item = response;
                //self.router.navigate(['edit-item', self.item.code])
            },

            onError(exception: Error) {
                console.error(exception);
            }
        })
    }

    back(): void {
        this.location.back()
    }
}

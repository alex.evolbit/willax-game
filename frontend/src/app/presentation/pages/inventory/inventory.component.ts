import {Component, OnInit, ViewChild, AfterViewInit, OnDestroy, ElementRef} from '@angular/core';
import {Router} from '@angular/router';
import {Item} from "../../../domain/types/Item";
import {GetItemsUseCase} from "../../../domain/usecase/GetItemsUseCase";
import {Callback} from "../../../domain/extra/UseCase";
import {DeleteItemUseCase} from "../../../domain/usecase/DeleteItemUseCase";
import {MatSnackBar} from "@angular/material/snack-bar";
import {MatDialog} from "@angular/material/dialog";
import {StandardDialogComponent} from "../../components/standard-dialog/standard-dialog.component";
import {MatTable, MatTableDataSource} from "@angular/material/table";
import {Params} from "../../../domain/usecase/GetItemsUseCase";
import {PageEvent} from "@angular/material/paginator";
import {GetItemsResponse} from "../../../domain/types/GetItemsResponse";
import {DeleteItemsUseCase} from "../../../domain/usecase/DeleteItemsUseCase";
import {MatSort, Sort} from "@angular/material/sort";
import {LiveAnnouncer} from "@angular/cdk/a11y";


interface Row {
    selected: boolean,
    item: Item
}

declare const onScan: any;

@Component({
    selector: 'app-inventory',
    templateUrl: './inventory.component.html',
    styleUrls: ['./inventory.component.scss']
})
export class InventoryComponent implements OnInit, OnDestroy, AfterViewInit {
    displayedColumns: string[] = ['code', 'name', 'price', 'brand', 'supplier', 'options'];
    dataSource = new MatTableDataSource([] as Row[]);
    search = "";

    page = 1;
    perPage = 10;
    total = 0;
    pageSizeOptions: number[] = [5, 10, 25, 100];
    sortCol = '';
    sortDir = '';

    private readonly scanListener;

    @ViewChild('searchInput')
    searchInput = {} as ElementRef;

    @ViewChild(MatTable)
    table = {} as MatTable<Row>;

    @ViewChild(MatSort)
    sort = {} as MatSort;

    constructor(
        private router: Router,
        private snackBar: MatSnackBar,
        public dialog: MatDialog,
        private liveAnnouncer: LiveAnnouncer,
        private getItemsUseCase: GetItemsUseCase,
        private deleteItemUseCase: DeleteItemUseCase,
        private deleteItemsUseCase: DeleteItemsUseCase,
    ) {
        const self = this;
        this.scanListener = function (event: any) {
            if (event.detail.scanCode) {
                self.searchInput.nativeElement.value = event.detail.scanCode;
                self.getItems(event.detail.scanCode);
            }
        };
    }

    ngOnInit(): void {
        this.getItems("");

        // Scan
        onScan.attachTo(document);
        document.addEventListener('scan', this.scanListener);
    }

    ngOnDestroy(): void {
        // Scan
        document.removeEventListener('scan', this.scanListener);
        onScan.detachFrom(document);
    }

    ngAfterViewInit() {
        this.dataSource.sort = this.sort;
    }

    onSearch() {
        this.getItems(this.search);
    }

    onItemClicked(item: Item) {
        //alert(el.code)
        this.router.navigate(['edit-item', item.code])
    }

    onItemDelete(item: Item) {
        const dialogRef = this.dialog.open(StandardDialogComponent, {
            data: {
                title: 'Eliminar',
                content: '¿Estás seguro de eliminar el item?',
            },
        });
        dialogRef.afterClosed().subscribe(result => {
            if (result)
                this.deleteItem(item);
        });
    }

    getItems(search: string) {
        const self = this;

        const params = new Params(search, this.page, this.perPage, this.sortCol, this.sortDir);

        this.getItemsUseCase.build(params, {
            onSuccess(response: GetItemsResponse) {
                self.dataSource.data = response.data.map(el => {
                    return {
                        selected: false,
                        item: el
                    } as Row;
                });
                self.perPage = response.pagination.perPage;
                self.page = response.pagination.page;
                self.total = response.pagination.total;
            },

            onError(exception: Error) {
                console.error(exception);
            }
        } as Callback<GetItemsResponse>);
    }

    deleteItem(item: Item) {
        const self = this;
        const ind = this.dataSource.data.findIndex(el => el.item.code == item.code);

        this.deleteItemUseCase.build(item.code, {
            onSuccess() {
                const data = self.dataSource.data;
                data.splice(ind, 1);
                self.dataSource.data = data;
                self.table.renderRows();
                self.snackBar.open('Eliminado', 'Cerrar', {duration: 5000, verticalPosition: 'top'});
            },

            onError(exception: Error) {
                console.error(exception);
            }
        } as Callback<void>);
    }

    onPage(event: PageEvent) {
        this.perPage = event.pageSize;
        this.page = event.pageIndex + 1;
        this.getItems(this.search);
    }

    onDeletedSelectedItems() {
        const dialogRef = this.dialog.open(StandardDialogComponent, {
            data: {
                title: 'Eliminar',
                content: '¿Estás seguro de eliminar los items?',
            },
        });
        dialogRef.afterClosed().subscribe(result => {
            if (result)
                this.deleteSelectedItems();
        });
    }

    deleteSelectedItems() {
        const self = this;
        const selItems = this.dataSource.data.filter(el => el.selected);
        if (selItems.length > 0) {
            const ids = selItems.map(el => el.item.code);
            this.deleteItemsUseCase.build(ids, {
                onSuccess() {
                    self.snackBar.open('Eliminado', 'Cerrar', {duration: 5000, verticalPosition: 'top'});
                    self.getItems(self.search);
                },
                onError(exception: Error) {
                    console.error(exception);
                }
            } as Callback<void>)
        }
    }

    announceSortChange(sortState: Sort) {
        console.log("sortState", sortState);

        if (sortState.direction !== '') {
            this.sortCol = sortState.active;
            this.sortDir = sortState.direction;
        } else {
            this.sortCol = '';
            this.sortDir = '';
        }
        this.getItems(this.search);


        /*if (sortState.direction) {
            this.liveAnnouncer.announce(`Sorted ${sortState.direction}ending`);
        } else {
            this.liveAnnouncer.announce('Sorting cleared');
        }*/
    }
}

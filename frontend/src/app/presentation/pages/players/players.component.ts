import {Component, OnInit, ViewChild} from '@angular/core';
import {GetPlayersUseCase} from "../../../domain/usecase/GetPlayersUseCase";
import {Callback} from "../../../domain/extra/UseCase";
import {User} from "../../../domain/types/User";
import {GetUserItemsUseCase} from "../../../domain/usecase/GetUserItemsUseCase";
import {ItemUser} from "../../../domain/types/ItemUser";
import {DeleteUserIdUseCase} from "../../../domain/usecase/DeleteUserIdUseCase";
import {MatTable} from "@angular/material/table";
import {MatDialog} from "@angular/material/dialog";
import {StandardDialogComponent} from "../../components/standard-dialog/standard-dialog.component";

interface Match {
    user: User,
    itemUsers: ItemUser[]
}

@Component({
    selector: 'app-players',
    templateUrl: './players.component.html',
    styleUrls: ['./players.component.scss']
})
export class PlayersComponent implements OnInit {
    displayedColumns: string[] = ['username', 'score', 'options'];

    matches: Match[] = [];

    @ViewChild(MatTable)
    table = {} as MatTable<Match>;

    constructor(
        private getPlayersUseCase: GetPlayersUseCase,
        private getUserItemsUseCase: GetUserItemsUseCase,
        private deleteUserIdUseCase: DeleteUserIdUseCase,
        public dialog: MatDialog,
    ) {
    }

    ngOnInit(): void {
        this.loadUsers();
    }

    private loadUsers() {
        const self = this;

        this.getPlayersUseCase.build(null, {
            onSuccess(items: User[]) {
                self.matches = items.map(user => {
                    self.getUserItems(user.id);
                    return {
                        user: user,
                        itemUsers: []
                    } as Match
                });
            },
            onError(err: Error) {
                console.log(err);
            }
        } as Callback<User[]>)
    }

    private getUserItems(userId: string) {
        const self = this;

        this.getUserItemsUseCase.build(userId, {
            onSuccess(items: ItemUser[]) {
                self.addItemsToUser(userId, items);
            },
            onError(err: Error) {
                console.log(err);
            }
        } as Callback<ItemUser[]>)
    }

    private addItemsToUser(userId: string, itemUsers: ItemUser[]) {
        const match = this.matches.find(el => el.user.id == userId);
        if (match) {
            match.itemUsers = itemUsers;
        }
    }

    calculateTotal(itemsUsers: ItemUser[]) {
        let total = 0;
        for (const itemsUser of itemsUsers) {
            total += itemsUser.item.price;
        }

        return total.toFixed(2);
    }

    onReloadList() {
        this.loadUsers();
    }

    onDeletePlayer(userId: string) {
        const dialogRef = this.dialog.open(StandardDialogComponent, {
            data: {
                title: 'Eliminar',
                content: '¿Estás seguro de eliminar el particanter?',
            },
        });
        dialogRef.afterClosed().subscribe(result => {
            if (result)
                this.deletePlayer(userId);
        });
    }

    private deletePlayer(userId: string) {
        const self = this;
        this.deleteUserIdUseCase.build(userId, {
            onSuccess() {
                const ind = self.matches.findIndex(item => item.user.id == userId);
                if (ind >= 0) {
                    self.matches.splice(ind, 1);
                    self.table.renderRows();
                }
            },
            onError(err: Error) {
                console.log(err);
            }
        } as Callback<void>)
    }
}

import {Injectable} from '@angular/core';
import {environment} from "../../../environments/environment";
import {Params as ParamsSync, SyncScoreUseCase} from "../../domain/usecase/SyncScoreUseCase";
import {Callback} from "../../domain/extra/UseCase";

@Injectable({
    providedIn: 'root'
})
export class ConfigService {

    private ftpSync = false;
    private userId = "";
    private filename = "";
    private intervalRef = 0;
    private ftpStatusCallback = {} as Callback<void>;

    constructor(
        private syncScoreUseCase: SyncScoreUseCase,
    ) {
    }

    isFtpSync() {
        return this.ftpSync;
    }

    startFtpSync(userId: string, filename: string, callback: Callback<void>) {
        this.userId = userId;
        this.filename = filename;
        this.ftpStatusCallback = callback;

        if (this.intervalRef > 0) {
            this.stopFtpSync();
        }

        this.ftpSync = true;
        this.syncScore();
        this.intervalRef = setInterval(() => {
            this.syncScore();
        }, environment.sync_interval_sec);
    }

    stopFtpSync() {
        clearInterval(this.intervalRef);
        this.intervalRef = 0;
        this.ftpSync = false;
    }

    private syncScore() {
        const params = {
            userId: this.userId,
            filename: this.filename
        } as ParamsSync;
        this.syncScoreUseCase.build(params, this.ftpStatusCallback)
    }
}

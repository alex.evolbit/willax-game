import {Component, OnInit, Inject} from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

export interface StandardDialogData {
    title: string;
    content: string;
}

@Component({
    selector: 'app-standard-dialog',
    templateUrl: './standard-dialog.component.html',
    styleUrls: ['./standard-dialog.component.scss']
})
export class StandardDialogComponent implements OnInit {

    constructor(
        public dialogRef: MatDialogRef<StandardDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public data: StandardDialogData
    ) {
    }

    ngOnInit(): void {
    }
}

import {Component, Input, OnInit} from '@angular/core';
import {environment} from "../../../../environments/environment";

@Component({
    selector: 'app-side-nav',
    templateUrl: './side-nav.component.html',
    styleUrls: ['./side-nav.component.scss']
})
export class SideNavComponent implements OnInit {

    @Input('user-id')
    userId: string = "";

    constructor() {
    }

    ngOnInit(): void {
    }

    onDownloadExcel() {
        window.open(environment.api_server + '/players/downloadExcel', '_self');
    }
}

# WillaxGame

## Requermientos

- Node JS
- Mysql Server

## Requermientos

Se requiere configurar el archivo config.json ubicado en la carpeta backend/config para la conexión con el servidor de base de datos.

Para esto debemos insertar los valores de conexión en la sección production.

## Compilación

Dentro de la carpeta frontend, debemos ejecutar el siguiente comando para generar la carpeta build,
    
    npm install && npm run build

Dentro de la carpeta backend, debemos ejecutar el siguiente comando para generar las tablas en base de datos

    npm install && npx sequelize-cli db:migrate

## Ejecutar servidor en modo desarrollo

Dentro de la carpeta backend, ejecutar: 

    npm run start

y navegar a `http://localhost:8080/`


Dentro de la carpeta frontend, ejecutar: 

    npm run start

y navegar a `http://localhost:5240/`


## Ejecutar servidor en modo producción (Linux)
    
Dentro de la carpeta backend, ejecutar: 
    
    pm2 start ecosystem.config.js
    pm2 save
    pm2 startup

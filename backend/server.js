const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');
const cors = require('cors');
const itemsRouter = require('./routes/items');
const usersRouter = require('./routes/users');

const root = './';
const app_folder = path.join(root, '../frontend/dist/willax-game');
console.log(app_folder);
const port = process.env.PORT || 8080;
const app = express();

app.use(bodyParser.json());
app.use(cors());
//app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static(app_folder, {
    etag: false,
    maxAge: '5000'
}));

app.use('/api/items', itemsRouter);
app.use('/api/players', usersRouter);

app.get('*', (req, res) => {
    res.status(200).sendFile(`/`, {root: app_folder});
});

app.listen(port, () => console.log(`API running on localhost:${port}`));

'use strict';
const {
    Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    class ItemUser extends Model {
        /**
         * Helper method for defining associations.
         * This method is not a part of Sequelize lifecycle.
         * The `models/index` file will call this method automatically.
         */
        static associate(models) {
            this.belongsTo(models.User, {foreignKey: 'userid'});
            this.belongsTo(models.Item, {foreignKey: 'itemcode'});
        }
    }

    ItemUser.init({
        userid: DataTypes.STRING,
        itemcode: DataTypes.STRING,
        price: DataTypes.FLOAT
    }, {
        sequelize,
        modelName: 'ItemUser'
    });
    return ItemUser;
};

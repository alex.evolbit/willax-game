'use strict';
const {
    Model,
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    class User extends Model {
        /**
         * Helper method for defining associations.
         * This method is not a part of Sequelize lifecycle.
         * The `models/index` file will call this method automatically.
         */
        static associate(models) {
            // define association here
        }
    }

    User.init({
        username: DataTypes.STRING,
        is_live: {
            type: DataTypes.BOOLEAN,
            defaultValue: false
        },
        is_ftp_sync: {
            type: DataTypes.BOOLEAN,
            defaultValue: false
        },
        score_filename: DataTypes.STRING
    }, {
        sequelize,
        modelName: 'User',
        timestamps: false
    });
    return User;
};

module.exports = {
    apps: [{
        name: "willax-game",
        script: "./server.js",
        instances: "max",
        exec_mode: "cluster",
        env: {
            "NODE_ENV": "production"
        }
    }]
}

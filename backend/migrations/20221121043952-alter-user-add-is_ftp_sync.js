'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
    async up(queryInterface, Sequelize) {
        return queryInterface.sequelize.transaction(t => {
            return Promise.all([
                queryInterface.addColumn('Users', 'is_ftp_sync', {
                    type: Sequelize.DataTypes.BOOLEAN,
                    defaultValue: false
                }, {transaction: t})
            ]);
        });
    },

    async down(queryInterface, Sequelize) {
        return queryInterface.sequelize.transaction(t => {
            return Promise.all([
                queryInterface.removeColumn('Users', 'is_ftp_sync', {transaction: t})
            ]);
        });
    }
};

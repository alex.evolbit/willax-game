'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
    async up(queryInterface, Sequelize) {
        return queryInterface.sequelize.transaction(t => {
            return Promise.all([
                queryInterface.addColumn('ItemUsers', 'createdAt', {
                    type: Sequelize.DataTypes.DATE,
                    defaultValue: new Date()
                }, {transaction: t}),
                queryInterface.addColumn('ItemUsers', 'updatedAt', {
                    type: Sequelize.DataTypes.DATE,
                    defaultValue: new Date()
                }, {transaction: t})
            ]);
        });
    },

    async down(queryInterface, Sequelize) {
        return queryInterface.sequelize.transaction(t => {
            return Promise.all([
                queryInterface.removeColumn('ItemUsers', 'createdAt', {transaction: t}),
                queryInterface.removeColumn('ItemUsers', 'updatedAt', {transaction: t})
            ]);
        });
    }
};

'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
    async up(queryInterface, Sequelize) {
        return queryInterface.sequelize.transaction(t => {
            return Promise.all([
                queryInterface.addColumn('Items', 'createdAt', {
                    type: Sequelize.DataTypes.DATE,
                    defaultValue: new Date()
                }, {transaction: t}),
                queryInterface.addColumn('Items', 'updatedAt', {
                    type: Sequelize.DataTypes.DATE,
                    defaultValue: new Date()
                }, {transaction: t})
            ]);
        });
    },

    async down(queryInterface, Sequelize) {
        return queryInterface.sequelize.transaction(t => {
            return Promise.all([
                queryInterface.removeColumn('Items', 'createdAt', {transaction: t}),
                queryInterface.removeColumn('Items', 'updatedAt', {transaction: t})
            ]);
        });
    }
};

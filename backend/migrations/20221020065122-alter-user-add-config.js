'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
    async up(queryInterface, Sequelize) {
        return queryInterface.sequelize.transaction(t => {
            return Promise.all([
                queryInterface.addColumn('Users', 'is_live', {
                    type: Sequelize.DataTypes.BOOLEAN,
                    defaultValue: false
                }, {transaction: t}),
                queryInterface.addColumn('Users', 'score_filename', {
                    type: Sequelize.DataTypes.STRING
                }, {transaction: t})
            ]);
        });
    },

    async down(queryInterface, Sequelize) {
        return queryInterface.sequelize.transaction(t => {
            return Promise.all([
                queryInterface.removeColumn('Users', 'is_live', {transaction: t}),
                queryInterface.removeColumn('Users', 'score_filename', {transaction: t})
            ]);
        });
    }
};

'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
    async up(queryInterface, Sequelize) {
        return queryInterface.sequelize.transaction(t => {
            return Promise.all([
                queryInterface.addColumn('ItemUsers', 'price', {
                    type: Sequelize.DataTypes.FLOAT,
                    defaultValue: 0
                }, {transaction: t})
            ]);
        });
    },

    async down(queryInterface, Sequelize) {
        return queryInterface.sequelize.transaction(t => {
            return Promise.all([
                queryInterface.removeColumn('ItemUsers', 'price', {transaction: t})
            ]);
        });
    }
};

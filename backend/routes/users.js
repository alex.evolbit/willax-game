const express = require('express');
const router = express.Router();
const {Op, QueryTypes} = require("sequelize");
const {save_local_file, create_local_file} = require('../lib/sync');
const {User, Item, ItemUser, LogScan, sequelize} = require('../models');
const excelJS = require("exceljs");


router.get('/', (req, res) => {
    User.findAll({
        attributes: ['id', 'username']
    }).then(users => {
        res.status(200).send({data: users});
    });
});

router.delete('/:id', async (req, res) => {
    let id = req.params.id;

    await ItemUser.destroy({
        where: {
            userId: id
        }
    });

    User.destroy({
        where: {
            id: id
        }
    }).then(affectedCount => {
        res.status(200).send({message: "Listo"});
    }).catch(() => {
        res.status(404).send({message: "Se produjo un error"});
    });
});

router.get('/:id/username', (req, res) => {
    let id = req.params.id;

    User.findByPk(id, {
        attributes: ['id', 'username']
    }).then(user => {
        if (user) {
            res.status(200).send({data: user.username});
        } else {
            res.status(404).send({message: "No se encontró elemento"});
        }
    }).catch(reason => {
        res.status(404).send({message: "Se produjo un error"});
    });
});

router.post('/:id/username', (req, res) => {
    let id = req.params.id;
    let data = req.body;

    User.update({username: data.username}, {
        where: {
            id: id
        }
    }).then((affectedCount) => {
        if (affectedCount > 0) {
            res.status(200).send({message: "Listo"});
        } else {
            User.create({id, username: data.username})
                .then(() => {
                    res.status(200).send({message: "Listo"});
                })
                .catch(() => {
                    res.status(404).send({message: "No se pudo crear"});
                })
        }
    }).catch(() => {
        res.status(404).send({message: "No se pudo actualizar"});
    });
});

router.get('/:id/config', (req, res) => {
    let id = req.params.id;

    User.findByPk(id, {
        attributes: ['is_live', 'score_filename', 'username', 'is_ftp_sync']
    }).then(user => {
        if (user) {
            res.status(200).send({data: user});
        } else {
            res.status(404).send({message: "No se encontró elemento"});
        }
    }).catch(reason => {
        res.status(404).send({message: "Se produjo un error"});
    });
});

router.post('/:id/config', async (req, res) => {
    let id = req.params.id;
    let data = req.body;

    const att = Object.keys(data).reduce(function (obj, k) {
        const cols = ['is_live', 'score_filename', 'username', 'is_ftp_sync'];
        if (cols.includes(k)) obj[k] = data[k];
        return obj;
    }, {});

    if (att.is_ftp_sync) {
        const user = await User.findByPk(id, {
            attributes: ['is_live', 'score_filename', 'username', 'is_ftp_sync']
        });

        if (!user.score_filename || user.score_filename.trim().length == 0) {
            res.status(404).send({message: "No se tiene el nombre de un archivo."});
            return;
        }
    }

    if (att.score_filename && att.score_filename.length > 0) {
        console.log("att.score_filename", att.score_filename);
        create_local_file(att.score_filename, function () {
            //res.status(200).send({message: "done"});
        });
    }

    User.update(att, {
        where: {
            id: id
        }
    }).then(async (affectedCount) => {
        if (affectedCount > 0) {
            if (att.is_ftp_sync || att.username) {
                await updateTxt(id);
            }
            res.status(200).send({message: "Listo"});
        } else {
            User.create(att)
                .then(() => {
                    res.status(200).send({message: "Listo"});
                })
                .catch(() => {
                    res.status(404).send({message: "No se pudo crear"});
                })
        }
    }).catch(() => {
        res.status(404).send({message: "No se pudo actualizar"});
    });
});

router.get('/:id/items', (req, res) => {
    let id = req.params.id;

    ItemUser.findAll({
        attributes: ['id', 'userid', 'itemcode'],
        where: {
            userid: {
                [Op.eq]: id
            }
        },
        order: [['id', 'desc']],
        include: Item
    }).then((itemUsers) => {
        const items = [];
        for (const itemUser of itemUsers) {
            items.push({id: itemUser.id, item: itemUser.Item});
        }
        res.status(200).send({data: items});
    }).catch(() => {
        res.status(404).send({message: "Se produjo un error"});
    });
});

router.get('/:id/score', async (req, res) => {
    let id = req.params.id;

    const user = await User.findByPk(id, {
        attributes: ['is_live', 'score_filename', 'username', 'is_ftp_sync']
    });

    ItemUser.findAll({
        attributes: ['id', 'price', 'itemcode'],
        where: {
            userid: id
        }
    }).then((itemUsers) => {
        res.status(200).send({
            data: {
                itemUsers,
                user
            }
        });
    }).catch(() => {
        res.status(404).send({message: "Se produjo un error"});
    });
});

router.post('/:id/items', (req, res) => {
    let id = req.params.id;
    let data = req.body;

    LogScan.create({code: data.code});

    Item.findByPk(data.code, {
        attributes: ['code', 'name', 'price', 'brand', 'supplier']
    }).then(item => {
        if (item) {
            ItemUser.create({userid: id, itemcode: data.code, price: item.price})
                .then(async (itemUser) => {

                    await updateTxt(id);

                    res.status(200).send({data: item, id: itemUser.id});
                })
                .catch(() => {
                    res.status(404).send({message: "No se pudo crear"});
                });
        } else {
            res.status(404).send({message: "No se encontró elemento"});
        }
    }).catch(reason => {
        res.status(404).send({message: "Se produjo un error"});
    });
});

updateTxt = async (id) => {
    // Escribiendo al txt
    const user = await User.findByPk(id, {
        attributes: ['username', 'is_ftp_sync', 'score_filename']
    });
    if (user && user.is_ftp_sync) {
        ItemUser.findAll({
            attributes: ['id', 'userid', 'itemcode', 'price'],
            where: {
                userid: id
            }
        }).then((itemUsers) => {
            let total = 0;
            for (const itemUser of itemUsers) {
                total += itemUser.price;
            }
            total = total.toFixed(2);
            const content = `${user.username}\n\n${total}`;
            save_local_file(user.score_filename, content, function () {
                //res.status(200).send({message: "done"});
            });
        }).catch((error) => {
            console.log(error);
            //res.status(404).send({message: "Se produjo un error"});
        });
    }
};

router.delete('/:userId/items/:itemUserId', (req, res) => {
    const userId = req.params.userId;
    const itemUserId = req.params.itemUserId;

    ItemUser.destroy({
        where: {
            [Op.and]: [
                {
                    userId: {
                        [Op.eq]: userId
                    }
                },
                {
                    id: {
                        [Op.eq]: itemUserId
                    }
                }
            ]
        }
    }).then(async (affectedCount) => {
        await updateTxt(userId);
        res.status(200).send({message: "Listo"});
    }).catch(() => {
        res.status(404).send({message: "Se produjo un error"});
    });
});

router.delete('/:id/items', (req, res) => {
    let id = req.params.id;

    ItemUser.destroy({
        where: {
            userid: {
                [Op.eq]: id
            }
        }
    }).then(async (affectedCount) => {
        await updateTxt(id);
        res.status(200).send({message: "Listo"});
    }).catch(() => {
        res.status(404).send({message: "Se produjo un error"});
    });
});

router.post('/:userId/sync', (req, res) => {
    const userId = req.params.userId;
    let data = req.body;

    if (data.filename) {
        User.findByPk(userId, {
            attributes: ['id', 'username']
        }).then(user => {
            if (user) {
                ItemUser.findAll({
                    attributes: ['id', 'userid', 'itemcode'],
                    where: {
                        userid: {
                            [Op.eq]: userId
                        }
                    },
                    include: Item
                }).then((itemUsers) => {
                    let total = 0;
                    for (const itemUser of itemUsers) {
                        total += itemUser.Item.price;
                    }
                    total = total.toFixed(2);
                    const content = `${user.username}\n\n${total}`;
                    save_local_file(data.filename, content, function () {
                        res.status(200).send({message: "done"});
                    });

                    /*const items = [];
                    for (const itemUser of itemUsers) {
                        items.push({id: itemUser.id, item: itemUser.Item});
                    }
                    res.status(200).send({data: items});*/
                }).catch(() => {
                    res.status(404).send({message: "Se produjo un error"});
                });
            } else {
                res.status(404).send({message: "No se encontró usuario"});
            }
        }).catch(reason => {
            res.status(404).send({message: "Se produjo un error"});
        });
    } else {
        res.status(404).send({message: "No se envió nombre de archivo"});
    }
});

router.get("/downloadExcel", async (req, res) => {
    const workbook = new excelJS.Workbook();
    const worksheet = workbook.addWorksheet("Juego");

    worksheet.columns = [
        {header: "Equipo", key: "team", width: 20},
        {header: "Código", key: "product_code", width: 20},
        {header: "Producto", key: "product_name", width: 60},
        {header: "Precio", key: "product_price", width: 10},
        {header: "Marca", key: "product_brand", width: 20},
        {header: "Proveedor", key: "product_supplier", width: 20}
    ];

    const data = await ItemUser.findAll({
        attributes: ['id', 'userid', 'itemcode'],
        order: [
            ['userid', 'asc'],
            ['itemcode', 'asc']
        ],
        include: [Item, User]
    });

    const rows = data.map(itemUser => {
        return {
            "team": itemUser.User.username,
            "product_code": itemUser.Item.code,
            "product_name": itemUser.Item.name,
            "product_price": itemUser.Item.price,
            "product_brand": itemUser.Item.brand,
            "product_supplier": itemUser.Item.supplier,
        };
    });
    worksheet.addRows(rows);

    try {
        res.setHeader(
            "Content-Type",
            "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
        );
        res.setHeader(
            "Content-Disposition",
            "attachment; filename=" + "juego.xlsx"
        );
        return workbook.xlsx.write(res).then(function () {
            res.status(200).end();
        });
    } catch (err) {
        res.send({
            status: "error",
            message: "Something went wrong",
        });
    }
});

module.exports = router;

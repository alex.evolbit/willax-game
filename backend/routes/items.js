const express = require('express');
const router = express.Router();
const {Op} = require("sequelize");
const {Item} = require('../models');

router.get('/', (req, res) => {
    const search = req.query.search;
    let page = req.query.page || 1;
    let perPage = req.query.perPage || 10;
    let sortCol = req.query.sortCol || null;
    let sortDir = req.query.sortDir || null;

    if (page <= 0) page = 1;
    if (perPage > 100) perPage = 100;

    page = parseInt(page);
    perPage = parseInt(perPage);
    const start = (page - 1) * perPage;

    let params = {};
    if (search) {
        params = {
            [Op.or]: [
                {
                    code: {
                        [Op.eq]: search
                    }
                },
                {
                    name: {
                        [Op.like]: `%${search}%`
                    }
                },
                {
                    brand: {
                        [Op.like]: `%${search}%`
                    }
                },
                {
                    supplier: {
                        [Op.like]: `%${search}%`
                    }
                }
            ]
        }
    }
    let order = [];
    let sortCols = ['code', 'name', 'price', 'brand', 'supplier'];
    let sortDirs = ['asc', 'desc'];
    if (sortCol && sortDir && sortCols.includes(sortCol.toLowerCase()) && sortDirs.includes(sortDir.toLowerCase())) {
        order.push([sortCol, sortDir]);
    }


    Item.findAndCountAll({
        where: params,
        offset: start,
        limit: perPage,
        order: order
    }).then((result) => {
        res.status(200).send({data: result.rows, pagination: {page, perPage, total: result.count}});
    }).catch(() => {
        res.status(404).send({message: "Se produjo un error"});
    });
});

router.get('/:id', (req, res) => {
    let id = req.params.id;

    Item.findByPk(id, {
        attributes: ['code', 'name', 'price', 'brand', 'supplier']
    }).then(result => {
        if (result) {
            res.status(200).send({data: result});
        } else {
            res.status(404).send({message: "No se encontró elemento"});
        }
    }).catch(reason => {
        res.status(404).send({message: "Se produjo un error"});
    });
});

router.post('', (req, res) => {
    let item = req.body;

    Item.create(item)
        .then(() => {
            res.status(200).send({message: "Listo"});
        })
        .catch(() => {
            res.status(404).send({message: "No se pudo crear"});
        });
});

router.put('/:id', (req, res) => {
    let id = req.params.id;
    let data = req.body;

    Item.update(data, {
        where: {
            code: id
        }
    }).then((affectedCount) => {
        if (affectedCount > 0) {
            res.status(200).send({message: "Listo"});
        } else {
            res.status(404).send({message: "No se encontró elemento"});
        }
    }).catch(() => {
        res.status(404).send({message: "No se pudo actualizar"});
    });
});

router.delete('/:id', (req, res) => {
    let id = req.params.id;

    Item.destroy({
        where: {
            code: {
                [Op.like]: id
            }
        }
    }).then(affectedCount => {
        if (affectedCount > 0)
            res.status(200).send({message: "Listo"});
        else
            res.status(404).send({message: "No se encontró elemento"});
    }).catch(() => {
        res.status(404).send({message: "Se produjo un error"});
    });
});

router.delete('', (req, res) => {
    let ids = req.body;

    Item.destroy({
        where: {
            code: {
                [Op.in]: ids
            }
        }
    }).then(affectedCount => {
        if (affectedCount > 0)
            res.status(200).send({message: "Listo"});
        else
            res.status(404).send({message: "No se encontró elemento"});
    }).catch(() => {
        res.status(404).send({message: "Se produjo un error"});
    });
});

router.post('/import', (req, res) => {
    let data = req.body;

    const ids = data.map(el => {
        return el.code.toString()
    });

    Item.findAll({
        attributes: ['code', 'name', 'price', 'brand', 'supplier'],
        where: {
            code: {
                [Op.in]: ids
            }
        }
    }).then((items) => {
        const result = [];
        for (const el of data) {
            const item = items.find(item => item.code === el.code.toString());
            el.isNew = item == null;
            result.push(el);
        }
        res.status(200).send({data: result});
    }).catch(() => {
        res.status(404).send({message: "Se produjo un error"});
    });
});

router.post('/save-import', (req, res) => {
    let data = req.body;

    const ids = data.map(el => {
        return el.code.toString()
    });
    Item.findAll({
        attributes: ['code', 'name', 'price', 'brand', 'supplier'],
        where: {
            code: {
                [Op.in]: ids
            }
        }
    }).then((items) => {
        for (const el of data) {
            const item = items.find(item => item.code === el.code.toString());
            if (item) {
                if (el.update) {
                    Item.update({name: el.name, price: el.price, brand: el.brand, supplier: el.supplier}, {
                        where: {
                            code: el.code
                        }
                    }).then((affectedCount) => {
                    }).catch(() => {
                    });
                }
            } else {
                el.code = el.code.toString();
                Item.create(el)
                    .then(() => {
                    })
                    .catch(() => {
                    });
            }
        }
        res.status(200).send({message: "Listo"});
    }).catch(() => {
        res.status(404).send({message: "Se produjo un error"});
    });
});


module.exports = router;

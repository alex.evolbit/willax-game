require('dotenv').config();

create_tmp_file = (content, cb) => {
    const fs = require('fs');
    const os = require('os');
    const path = require('path');

    const filename = new Date().getTime() + ".txt";
    const tmp_path = path.join(os.tmpdir(), filename);

    fs.writeFile(tmp_path, content, function () {
        cb(tmp_path);
    });
};

upload_ftp = (ori_path, dest_path, config, cb) => {
    const FtpClient = require('ftp');

    const c = new FtpClient();
    c.on('ready', function () {
        c.put(ori_path, dest_path, function (err) {
            if (err) throw err;
            c.end();
            cb();
        });
    });
    c.connect(config);
};

send_file = (filename, content, callback) => {
    create_tmp_file(content, function (path) {
        const ftp_config = {
            "host": process.env.FTP_HOST,
            "port": process.env.FTP_PORT,
            "user": process.env.FTP_USER,
            "password": process.env.FTP_PWD,
        };
        const dest_path = process.env.FTP_PATH + filename;
        upload_ftp(path, dest_path, ftp_config, callback);
    });
};

save_local_file = (filename, content, callback) => {
    const fs = require('fs');
    const path = require('path');
    const folder = path.join(__dirname, '../ftp');

    if (!fs.existsSync(folder)) {
        fs.mkdirSync(folder);
    }
    const file_path = path.join(folder, filename);

    fs.writeFile(file_path, content, function () {
        callback();
    });
};

create_local_file = (filename, callback) => {
    const fs = require('fs');
    const path = require('path');
    const folder = path.join(__dirname, '../ftp');

    if (!fs.existsSync(folder)) {
        fs.mkdirSync(folder);
    }
    const file_path = path.join(folder, filename);
    if (!fs.existsSync(file_path)) {
        fs.writeFile(file_path, "", function () {
            callback();
        });
    }
};

module.exports = {create_tmp_file, upload_ftp, send_file, save_local_file, create_local_file};



